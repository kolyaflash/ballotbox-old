import polls from "./polls/polls.module.js";
import config from "./app.config";
import "../scss/app.scss";

var app = angular.module('app', [
  'ui.router',
  polls.name,
])
.config(config)
.run(($rootScope) => {
  /*@ngInject*/
  $rootScope.$on('$stateChangeStart', (e, newUrl, oldUrl) => {
    if (newUrl !== oldUrl) {
      $rootScope.loadingView = true;
    }
  });
  $rootScope.$on('$stateChangeSuccess', () => {
    $rootScope.loadingView = false;
  });
  $rootScope.$on('$stateChangeError', () => {
    $rootScope.loadingView = false;
  });
});

export default app;
