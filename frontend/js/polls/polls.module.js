import states from './polls.states';
import view from './components/view/view.module.js';

var app = angular.module('app.polls', [
  view.name,
])
.config(states);

export default app;
