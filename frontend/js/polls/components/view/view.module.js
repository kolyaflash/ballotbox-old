import ctrl from './view.controller.js';

export default angular.module('app.polls.view', [
])
.controller('ViewPollController', ctrl);
