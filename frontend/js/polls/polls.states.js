/*@ngInject*/
export default ($stateProvider) => {
  $stateProvider
    .state('home', {
      url: '/',
      template: require('./components/view/view.html'),
      controller: 'ViewPollController',
      controllerAs: 'vm'
    });
}
