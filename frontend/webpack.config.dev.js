'use strict';

var path = require("path");
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');
var WebpackCleanupPlugin = require('webpack-cleanup-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

const getPath = (pathToFile) => path.resolve(__dirname, pathToFile);
//require('bootstrap-loader');



module.exports = {
    devtool: 'inline-source-map',

    // context: __dirname,
    // entry: ['bootstrap-loader', './assets/js/app.js'],
    entry: {
      app: [
        getPath('./js/core/init.js'),
      ],
      vendor: [
        getPath('./js/core/vendor.js'),
      ],
    },
    output: {
        path: getPath('../ballotbox/static/bundles/'),
        filename: "[name]-[hash].js"
        //publicPath: '/'

    },

    plugins: [
        new BundleTracker({filename: '.webpack-stats.json'}),
        new WebpackCleanupPlugin({quiet: true}),
        new webpack.HotModuleReplacementPlugin(),
        new ExtractTextPlugin({
          filename: '[name]-[hash].css',
          disable: false, allChunks: true
        }),
    ],

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loaders: ['ng-annotate-loader', 'babel-loader'],
            },
            {
                test: /\.scss$/,
                // use: [{
                //     loader: "style-loader" // creates style nodes from JS strings
                // }, {
                //     loader: "css-loader" // translates CSS into CommonJS
                // }, {
                //     loader: "sass-loader" // compiles Sass to CSS
                // }],
                use: ExtractTextPlugin.extract({
                  fallback: 'style-loader',
                  use: ['css-loader?sourceMap=true', 'sass-loader?sourceMap=true']
                }),
            },
            {
                test: /\.html$/,
                loader: 'raw-loader'
            },
        ]
    },
    resolve: {
      //modulesDirectories: ['web_modules', 'node_modules', 'bower_components'],
    }
};
