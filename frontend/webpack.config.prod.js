'use strict';

var path = require("path");
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');
var WebpackCleanupPlugin = require('webpack-cleanup-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

const getPath = (pathToFile) => path.resolve(__dirname, pathToFile);
//require('bootstrap-loader');


module.exports = {
    devtool: 'cheap-module-source-map',
    entry: {
      app: [
        getPath('./js/core/init.js'),
      ],
      vendor: [
        getPath('./js/core/vendor.js'),
      ],
    },
    output: {
        path: getPath('../ballotbox/static/bundles/'),
        filename: "[name]-[hash].js"
        //publicPath: '/'

    },

    plugins: [
        new BundleTracker({filename: '.webpack-stats.json'}),
        new WebpackCleanupPlugin({quiet: true}),
        new ExtractTextPlugin({
          filename: '[name]-[hash].css',
          disable: false, allChunks: false
        }),
        new webpack.optimize.AggressiveMergingPlugin(),
        new webpack.optimize.UglifyJsPlugin({
          compress: {
              screw_ie8: true,
              warnings: false
          },
          beautify: false,
          sourceMap: false,
          mangle: {
            // You can specify all variables that should not be mangled.
            // For example if your vendor dependency doesn't use modules
            // and relies on global variables. Most of angular modules relies on
            // angular global variable, so we should keep it unchanged
            except: ['$super', '$', 'exports', 'require', 'angular']
          }
        }),
    ],

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loaders: ['ng-annotate-loader', 'babel-loader?presets[]=es2015'],
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                  fallback: 'style-loader',
                  use: ['css-loader', 'sass-loader']
                }),
            },
            {
                test: /\.html$/,
                loader: 'raw-loader'
            },
        ]
    },
    resolve: {
      //modulesDirectories: ['web_modules', 'node_modules', 'bower_components'],
    }
};
